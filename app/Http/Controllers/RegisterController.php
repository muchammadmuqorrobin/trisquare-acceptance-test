<?php

namespace App\Http\Controllers;
use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Validator;
use Redirect;

class RegisterController extends Controller
{
    public function register()
    {
        $data = Country::all();
        return view('konten.users.register',['message'=>"welcome to register page",'countries'=>$data]);
    }
    
    public function adduser(Request $req)
    {
       
        
        $validator = Validator::make($req->all(), [
           'email'=>'required|unique:user'
        ]);
       
        
        if ($validator->fails()) {
           $message = "Email already used";
            $data = Country::all();
           return view('konten.users.register',['message'=>$message,'countries'=>$data]);
        } else {
            
             $user = new User;
            $user->nama = $req->name;
            $user->email = $req->email;
            $user->dob = $req->dob;        
            $user->country_id = $req->country;
            $user->password = $req->password; 
            $result = $user->save();
      
        if($result){
             $message = "Registration Successful.";   
            return view('konten.users.users',['message'=>$message]);
        }else{
           
             return back()->with('alert', 'Operation failed.');

        }
        }
          
       
        
    }
    
    
    public function getuser()
    {
        $users = User::join('country','user.country_id','=','country.country_id')
        ->get(['user.user_id','user.nama','user.email','user.dob','country.country']);
        $result = array(
            'data' => $users 
        );
        $users = json_encode($result);
        return $users;
    }




}