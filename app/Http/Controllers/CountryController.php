<?php

namespace App\Http\Controllers;
use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Validator;

class CountryController extends Controller
{
	public function countries()
	{
		return view('konten.users.countries');
	}
	
	function getcountries(){
	    $users = Country::join('user','user.country_id','=','country.country_id')
	    ->select('country.country',DB::raw('count(user.country_id) as jumlah'))
	    ->groupBy('country.country_id')
        ->get();
        $result = array(
            'data' => $users 
        );
        $users = json_encode($result);
        return $users;
	}
	



}