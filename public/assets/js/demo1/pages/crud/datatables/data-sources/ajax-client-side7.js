'use strict';
var KTDatatablesDataSourceAjaxClient = function() {
	var initTableUsers = function() {
		// var table = $('#table_users');
		// // begin first table
		// table.DataTable({
		var table = $('#table_users').DataTable({
			responsive: true,
			ajax: {
				url: 'api/getuser',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
		
			columns: [
			{data: 'no'},
			{data: 'nama'},
			{data: 'email'},
			{data: 'country'},
			{data: 'dob'},	

			],
			columnDefs: [
			{	
				targets: -5,
				title: 'no',
				orderable: true,
				"data": "user_id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},


			],
		});
	

	
	};
	
		var initTableCountries = function() {
		// var table = $('#table_users');
		// // begin first table
		// table.DataTable({
		var table = $('#table_country').DataTable({
			responsive: true,
			ajax: {
				url: 'api/getcountries',
				data: {
					pagination: {
						perpage: 50,
					},
				},
				contentType: "application/json; charset=utf-8"
			},
		
			columns: [
			{data: 'no'},
			{data: 'country'},
			{data: 'jumlah'},

			],
			columnDefs: [
			{	
				targets: -3,
				title: 'no',
				orderable: true,
				"data": "user_id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},


			],
		});
	

	
	};


	return {

		//main function to initiate the module
		init: function() {
			initTableUsers();
			initTableCountries();
		
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesDataSourceAjaxClient.init();
});