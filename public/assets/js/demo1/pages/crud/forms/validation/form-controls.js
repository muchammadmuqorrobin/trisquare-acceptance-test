// Class definition

var KTFormControls = function () {
    // Private functions
    
    var demo1 = function () {
        $( "#form_users" ).validate({
            // define validation rules
            rules: {
                username: {
                    required: true,
                    minlength: 5 
                },
                email: {
                    required: true,
                    email: true,
                    unique:user,
                    minlength: 10 
                },
                password: {
                    required: true,
                    minlength: 4  
                },
                nama: {
                    required: true,
                },
                level: {
                    required: true
                },

            },
            messages :{
                username:{
                    required :"Silahkan Isi Username",
                    minlength : "Minimal 5 Karakter"
                },
                password:{
                    required :"Silahkan Isi password",
                    minlength : "4 Minimal character"
                },
                nama:{
                    required :"Silahkan Isi Nama Anda",
                },
                level:{
                    required :"Silahkan Isi Level",
                },
                email:{
                    required :"Silahkan Isi email anda",
                    email :"Format email anda salah",
                },

            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
            },

            // submitHandler: function (form) {
            //     //form[0].submit(); // submit the form
            // }
        });       
    }

   
    return {
        // public functions
        init: function() {
            demo1(); 
            demo2();
            formsiswa();
        }
    };
}();

jQuery(document).ready(function() {    
    KTFormControls.init();
});