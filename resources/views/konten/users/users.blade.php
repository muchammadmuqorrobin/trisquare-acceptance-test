
	@extends('layout')

	@section('content')
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	
				<div class="alert alert-solid-success alert-bold" role="alert" style="margin-top: 20px">
                							<div class="alert-text">	{{ $message }}
                								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                									<span aria-hidden="true"><i class="la la-close"></i></span>
                								</button>
                							</div>
                						</div>
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand flaticon2-line-chart"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Data Users
						</h3>
					</div>
				
				</div>
				<div class="kt-portlet__body">

					<!--begin: Datatable -->
					<table class="table table-striped- table-bordered table-hover table-checkable" id="table_users">
						<thead>
							<tr>
								<th>no</th>
								<th>Name</th>
								<th>Email</th>
								<th>Country</th>
								<th>DOB</th>
							
							</tr>
						</thead>
					</table>

					<!--end: Datatable -->
				</div>
			</div>
		</div>
		<!-- end:: Content -->
	</div>


	@endsection