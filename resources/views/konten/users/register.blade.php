
@extends('layout')

@section('content')
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

					
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Register
												</h3>
											</div>
										</div>
										
										
								
                						<div class="alert alert-solid-danger alert-bold" role="alert" style="margin-top: 20px">
                							<div class="alert-text">	{{ $message }}
                								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                									<span aria-hidden="true"><i class="la la-close"></i></span>
                								</button>
                							</div>
                						</div>
					            
                                  
											
															
										<!--begin::Form-->
										<form class="kt-form kt-form--label-right" id="form_users" method="post" action="api/adduser">
										{!! csrf_field() !!}
											<div class="kt-portlet__body">
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Name *</label>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<input type="text" class="form-control" name="name" placeholder="Enter your name">
												
													</div>
												</div>
												
													<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<input type="text" class="form-control" name="email" placeholder="Enter your email">
													</div>
												</div>
												
												
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Password *</label>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<input type="password" class="form-control" name="password" placeholder="Enter your password">
													</div>
												</div>
												
													
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Date of Birth *</label>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<input type="Date" class="form-control" name="dob" placeholder="Enter your date of birth">
													</div>
												</div>
											
											
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Country *</label>
													<div class="col-lg-6 col-md-6 col-sm-12 form-group-sub">
														<select class="form-control kt-select2" id="add_user" name="country" required>
														@foreach ($countries as $c)
                                							<option value="{{ $c -> country_id }} "> 
                                								{{ $c -> country }}
                                							</option>
                                						@endforeach
														</select>
													</div>
												</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<div class="row">
														<div class="col-lg-9 ml-lg-auto">
															<button type="submit" class="btn btn-brand">Submit</button>
															<button type="reset" class="btn btn-secondary">Cancel</button>
														</div>
													</div>
												</div>
											</div>
										</form>

										<!--end::Form-->
									</div>

									<!--end::Portlet-->
<!-- 								</div>
							</div> -->
						</div>

						<!-- end:: Content -->
					</div>
@endsection