
	@extends('layout')

	@section('content')
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	
		
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand flaticon2-line-chart"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Countries
						</h3>
					</div>
				
				</div>
				<div class="kt-portlet__body">

					<!--begin: Datatable -->
					<table class="table table-striped- table-bordered table-hover table-checkable" id="table_country">
						<thead>
							<tr>
								<th>no</th>
								<th>Country</th>
								<th>Number of User</th>
							
							
							</tr>
						</thead>
					</table>

					<!--end: Datatable -->
				</div>
			</div>
		</div>
		<!-- end:: Content -->
	</div>


	@endsection